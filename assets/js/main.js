$(document).ready(function () {

    $(".call_back.btn, .order_btn.btn").on('click', function () {
        $(this).parent().parent().children().children().removeClass('active');
        $(this).addClass('active');
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top - 250
        }, {
            duration: 1000,
            easing: "swing"
        });
        return false;
    });

// Фиксированная шапка

    function scrolling() {
        if ($(window).scrollTop() >= ($('body').offset().top) + 1) {
            $('.header').addClass('header_fixed');
            $('.header').parent().addClass('header_fixed');
        } else {
            $('.header').removeClass('header_fixed');
            $('.header').parent().removeClass('header_fixed');
        }
    }

    if ($('body').length) {
        $(window).scroll(function () {
            scrolling();
        });
    }

    function scrollMenu() {
        var mobMenu = $('.nav_side');
        var headerHeight = $('.header').height();
        var windowHeight = $(window).height();
        if ($(window).width() < 1024) {
            var height = windowHeight - headerHeight;
            $(mobMenu).css("height", height);
        } else if ($(window).width() > 1024) {
            $(mobMenu).css('height', 'auto');
        }
    }

    $(window).on('load resize', function () {
        scrollMenu();
    });

// Dropdown Menu
    if ($(window).width() <= 1024) {
        function dropDownMenu() {

            $(".menu_list > .menu_item.has_dropdown").on('click', function () {
                $(this).toggleClass('active');
            });
        }
        $(window).on('load', function () {
            dropDownMenu();
        });
    }

    // Burger menu

    $('.burger_menu').on('click', function () {
            $('.burger_menu, .header').toggleClass('active');
            $('body').toggleClass('lock');
        }
    );

    // Модальное окно fancybox

    $(".modal").fancybox({
        padding: 10,
        autoFocus: false,
    });

// Табы

    $('.tabs_head').on('click', '.tab_item', function (e) {
        e.preventDefault();
        $(this).parent().children().removeClass('active');
        $(this).parent().parent().children('.tabs_body').removeClass('active');
        $(this).addClass('active');
        $(this).parent().parent().children('.tabs_body').eq($(this).index()).addClass('active');

    });

    /* OWL - carousel */
    var sliderList = $('.clients_slider');
    $(sliderList).addClass('owl-carousel');
    $(sliderList).owlCarousel({

        autoplay: false,
        nav: true,
        dots: false,
        items: 4,
        responsiveClass: true,
        responsive: {
            980: {
                items: 4
            },
            769: {
                items: 3
            },
            500: {
                items: 2
            },
            0: {
                items: 1
            }
        },
    });

    /* OWL - carousel */
    var mainSlider = $('.slider_list');
    $(mainSlider).addClass('owl-carousel');
    $(mainSlider).owlCarousel({
        autoplay: false,
        nav: false,
        dots: true,
        items: 1,
        responsiveClass: true
    });

});

// Menu Arrow
var menu =  document.querySelector('.menu_list');
var dropDownItem = menu.querySelectorAll('li.has_dropdown > span');
for(var i = 0; i < dropDownItem.length; i++){
    dropDownItem[i].insertAdjacentHTML('beforeend', '<svg viewBox="0 0 8 4" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
        '<path d="M4 3.81128L0.102886 0.0612793L7.89711 0.0612793L4 3.81128Z" />\n' +
        '</svg>\n');
}
